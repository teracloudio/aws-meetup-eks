output "VPC" {
  value = {
    vpc_id  = "${module.vpc.vpc_id}"
    private = ["${module.vpc.private_subnets}"]
    public  = ["${module.vpc.public_subnets}"]
  }
}

output "ecr" {
  value = "${aws_ecr_repository.ECR.repository_url}"
}

output "eks_cluster_id" {
  value = "${module.eks.cluster_id}"
}

output "eks_cluster_endpoint" {
  value = "${module.eks.cluster_endpoint}"
}

# output "kubeconfig" {
#   value = "${module.eks.kubeconfig}"
# }


# output "eks_config_map_aws_auth" {
#   value = "${local.config_map_aws_auth}"
# }

