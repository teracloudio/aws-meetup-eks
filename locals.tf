locals {
  project                   = "awsmeetup"
  cluster_name              = "awsmeetup"
  owner                     = "damian"
  environment               = "dev"
  region                    = "us-west-2"
  cidr                      = "10.0.0.0/16"
  public_subnets            = "10.0.0.0/20,10.0.16.0/20"
  private_subnets           = "10.0.32.0/24,10.0.48.0/20"
  domain_name               = "meetup.teracloud.io"
  key_name                  = "damian"
  workstation_external_cidr = "${chomp(data.http.workstation_external_ip.body)}/32"
}
