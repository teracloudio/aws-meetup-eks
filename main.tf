provider "aws" {
  region  = "${local.region}"
  version = "~> 1.34"
}

provider "http" {
  version = "~> 1.0"
}

data "aws_region" "current" {}

# this data source retrieves from http://icanhazip.com
# your workstation External IP to add it later to the
# security groups and any other place that needs IP
# validation
data "http" "workstation_external_ip" {
  url = "http://icanhazip.com"
}

#agregar modulo de teracloud
module "vpc" {
  source               = "./modules/vpc/"
  name                 = "${local.project}-vpc"
  cidr                 = "${local.cidr}"
  azs                  = "${data.aws_region.current.name}a,${data.aws_region.current.name}b"
  public_subnets       = "${local.public_subnets}"
  private_subnets      = "${local.private_subnets}"
  enable_dns_hostnames = true
  enable_dns_support   = true
  domain_name          = "${local.domain_name}"
  region               = "${data.aws_region.current.name}"
  env                  = "${local.environment}"
  owner                = "${local.owner}"
  vpn                  = ""
}

# todo agregar desired capacity
module "eks" {
  source       = "terraform-aws-modules/eks/aws"
  version      = "1.5.0"
  cluster_name = "${local.cluster_name}"
  subnets      = ["${split(",", module.vpc.private_subnets)}"]
  tags         = "${map("Environment", "test")}"
  vpc_id       = "${module.vpc.vpc_id}"
}

resource "aws_ecr_repository" "ECR" {
  name = "${local.project}_${local.environment}"
}
